#!/bin/bash

# define
readonly NAME=yourname
MY_DIR=./resources/$NAME/public

# create document root directory
mkdir -p $MY_DIR
echo '<?php

phpinfo();
' > $MY_DIR/index.php


# set my directory
cp ./docker/nginx/conf.d/default.conf.example ./docker/nginx/conf.d/default.conf
sed -i "s/MyName/$NAME/" ./docker/nginx/conf.d/default.conf
cp ./docker/php-fpm/Dockerfile.example ./docker/php-fpm/Dockerfile
sed -i "s/MyName/$NAME/" ./docker/php-fpm/Dockerfile
cp ./docker/nginx/Dockerfile.example ./docker/nginx/Dockerfile
sed -i "s/MyName/$NAME/" ./docker/nginx/Dockerfile
cp ./.vscode/launch.json.example ./.vscode/launch.json
sed -i "s/MyName/$NAME/g" ./.vscode/launch.json