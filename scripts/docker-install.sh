#! /bin/bash

# package install
sudo dnf -y install vim git wget unzip jq

# docker remove
cat /etc/redhat-release
sudo dnf -y update
sudo dnf -y remove docker \
                docker-client \
                docker-client-latest \
                docker-common \
                docker-latest \
                docker-latest-logrotate \
                docker-logrotate \
                docker-engine

# docker install
sudo dnf install -y yum-utils
sudo yum-config-manager \
    --add-repo \
    https://download.docker.com/linux/centos/docker-ce.repo

sudo dnf -y install docker-ce docker-ce-cli containerd.io

docker --version

#sudo systemctl start docker
sudo systemctl enable --now docker
#sudo systemctl status docker

# docker-compose install
# 最新バージョン確認：https://github.com/docker/compose/releases
DOCKER_COMPOSE_VERSION=$(curl -s https://api.github.com/repos/docker/compose/releases/latest | jq -r '.tag_name')

sudo curl -L "https://github.com/docker/compose/releases/download/$DOCKER_COMPOSE_VERSION/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
sudo usermod -aG docker $USER
newgrp docker
docker-compose --version

# alias settings
echo alias dc='docker-compose' >> ~/.bashrc
source ~/.bashrc

