<?php
class Lesson {
    public function practice1(array $_list):array {
        return $_list;
    }

    public function practice2(array $_list):array {
        $result['id'] = array_column($_list, 'id');
        return $result;
    }

    public function practice3(array $_list):array {
        $keys = ['id', 'name', 'male'];
        foreach ($keys as $key) {
            $result[$key] = array_column($_list, $key);
        }
        return $result;
    }

    public function practice4(array $_list):array {
        // キーを準備する
        $keys = [];
        foreach ($_list as $list) {
            $keys = array_merge($keys, array_keys($list));
        }
        $keys = array_unique($keys);

        // 値を入れる
        foreach ($keys as $key) {
            $result[$key] = array_column($_list, $key);
        }
        return $result;
    }
}

$list = [
    [
        'id'   => 1,
        'name' => "Hashimoto",
        'male' => true,
        'hoge' => "huga"
    ],
    [
        'id'   => 2,
        'name' => "Matsutama",
        'male' => true,
        'piyo' => "poyo"
    ],
    [
        'id'   => 3,
        'name' => "Maruyama",
        'male' => false,
        'piyo' => "mochi"
    ],
];

// $data = (new Lesson())->practice1($list);
// $data = (new Lesson())->practice2($list);
// $data = (new Lesson())->practice3($list);
$data = (new Lesson())->practice4($list);
echo '<pre style="font-size: 1.2rem">';
var_dump($data);