# Lesson 4

## 必須課題
**環境変数(.env)を取得するクラスの作成**
- .env ディレクトリ配下のファイルからそれぞれの環境に応じた環境変数を取得する
  - 環境を識別する値は `$_SERVER['APP_ENV']` から取得する

**要件**
- ディレクトリ構成は下記ようにする。
  ```
  プロジェクトルート(自分の名前のディレクトリ)/
      ┣━ .env/
      ┃    ┣━ local    // ローカル環境の.envファイル
      ┃    ┣━ staging  // 検証環境の.envファイル
      ┃    ┗━ product  // 本番環境の.envファイル
      ︙
   ```
- `.env` ディレクトリ配下のファイルは下記のように定義する。
  ```
  # KEY=value の形で定義する
  USER=user
  PASS=012=zaq!
  HOGE=fuga
  PIYO=puyo

  # php.ini の設定
  PHP_INI_MBSTRING.LANGUAGE=Japanses
  PHP_INI_UPLOAD_MAX_FILESIZE=64M
  ```
- 環境がローカル環境のとき、 `Env::get('HOGE')` で `.env/local` ファイルの `HOGE=hoge` 行の `HOGE` をキーとした値(`hoge`)を取得する
- 環境が検証環境のとき、 `Env::get('HOGE')` で `.env/staging` ファイルの `HOGE=hoge` 行の `HOGE` をキーとした値(`hoge`)を取得する
- 環境が本番環境のとき、 `Env::get('HOGE')` で `.env/product` ファイルの `HOGE=hoge` 行の `HOGE` をキーとした値(`hoge`)を取得する
- 値に = が入っていた場合も考慮する
- 前後に空白があった場合も考慮する
- コメントアウト(#から始まる行)の設定は読み込まない

## 追加課題
- PHP の設定を行えるようにする

**要件**
- .env ディレクトリ配下のファイルの設定でキーが `PHP_INI_` から始まる場合はPHPの設定値を記載する事とする
- `Env` クラスに `set()` メソッドを定義する。
  - `set()` メソッドは dispacher 等で呼び出しす。
  - `PHP_INI_` から始まるPHPの設定値を標準関数 `ini_set()` を用いて最初に設定する。

## ポイント
- nginx の設定で `fastcgi_param APP_ENV local` を設定すると、`$_SERVER['APP_ENV']` で設定した値(`local`)を取得できる
- ファイルを読み込むのはPHPの標準関数を利用するといい
  - ファイルを読み込む標準関数はたくさんあるので、用途をよく考えて使い分けましょう
- ファイルを読み込めない場合の例外処理を忘れないようにしましょう