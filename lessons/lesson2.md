# Lesson 2

## 必須課題
**DB 接続用のクラスを作成**
- PHP PDO を使用して DB 接続用のクラスを作成
- DB に接続でエラーが発生した場合のハンドリングを実装
- salto データベースにテーブルを作成する
  - テーブル名は自分の名前(hashimoto)で作成する
  - 作成したテーブルにデータを投入する
  - 上記で作成したクエリは `docker/mysql/init.d/` 配下に `01_tableName.sql` (01_hashimoto.sql)の形で作成する
- DB から取得したデータを画面に表示する

## 追加課題
- Singleton パターンによる実装
- Clone はさせないようにする

## ポイント
- PDO の使い方 → [PDO リファレンス](https://www.php.net/manual/ja/class.pdo.php)
- PDOException の使い方 → [PDOException リファレンス](https://www.php.net/manual/ja/class.pdoexception.php)
- Singleton とは
  - そのクラスのインスタンスが1つしか生成されないことを保証するデザインパターンのこと
  - 何度も呼び出す必要のあるインスタンスは Singleton で実装することでメモリの使用量を減らせる