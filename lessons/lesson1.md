# Lesson 1

## 必須課題
**開発環境を構築する**
1. `VirtualBox` と `Vagrant` を利用すること
2. `Vagrantfile` は下記の内容に全部書き換えること
   - <details><summary>Vagrantfile</summary>

        ``` ruby
        # -*- mode: ruby -*-
        # vi: set ft=ruby :

        VMNAME = 'work'

        Vagrant.configure("2") do |config|

        config.vm.box = "bento/almalinux-9"
        config.vm.hostname = VMNAME
        config.vm.network "private_network", ip: "192.168.33.10"
        # config.vm.network "forwarded_port", guest: 3306, host: 3306

        config.vm.provider "virtualbox" do |vb|
            vb.name = VMNAME
            vb.cpus = 2
            vb.memory = 1024
        end

        config.vm.provision "shell", inline: <<-SHELL
            sudo yum -y update
            sudo yum -y install git vim wget unzip zip net-tools
            # sudo dnf -y update kernel
            # sudo dnf -y install kernel-devel kernel-headers gcc gcc-c++
            # sudo dnf -y install libX11 libXt libXext libXmu
        SHELL

        end
        ```
    </details>

   - OSイメージは `bento/almalinux-9` を使用すること
   - プライベートIP は適宜変更して利用すること
3. *Git* を使用できる環境にする
   - [GitLab](https://gitlab.com/) に SSH キーを登録する
   - 下記のコマンドで 「Welcome to GitLab, UserName!」 が表示されること
        ```
        ssh -T git@gitlab.com
        ```
4. *Docker* と *Docker Compose* を使用できる環境にすること

## 追加課題
**Docker Compose でコンテナ群を作る**
1. *Nginx* コンテナを作成する → Web サーバ
2. *PHP-FPM* コンテナを作る → Application サーバ
3. *MySQL* コンテナを作る → Database サーバ

## ポイント
- お勧め VSCode の拡張機能
  - Remote SSH：入れて仮想環境のコードを直接操作できるようにすると作業がしやすい
  - PHP Intelephense：PHP のコードを書きやすくなる
  - PHP Debug：PHP でデバック実行ができるようになる
- 秘密鍵は `ssh-keygen` コマンドで作成できる
  - 何の鍵かを管理しやすいように鍵の名前を `gitlab.com` のようなホスト名にするのがお勧め
  - その場合、SSH 接続をしたときにデフォルトで鍵を読み込まないので、`config` ファイルを作成する必要がある
  - `config` ファイルは所有者だけ読み書きができる権限でないと Parmission Error になる
- Docker は root だけではなくユーザ権限(今回は vagrant )でも実行できるように設定をする
  - docker のグループに vagrant を追加して上げれば大丈夫