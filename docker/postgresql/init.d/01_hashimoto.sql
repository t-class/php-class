-- Change Database
\c salto

-- Create Schema
CREATE SCHEMA saltoschema;

-- Create Table
create table if not exists hashimoto (
    id serial not null primary key,
    name varchar(20) not null
);

-- Create Data
insert into hashimoto (name) values ("Anan"), ("Ikeda"), ("Imai"), ("Iwakiri"), ("Shiroshita"), ("Nakagawa"), ("Hashimoto"), ("Munemoto");