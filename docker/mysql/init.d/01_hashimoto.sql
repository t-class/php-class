use salto;

create table if not exists hashimoto (
    id int unsigned not null auto_increment primary key,
    name varchar(20) not null
);

insert into hashimoto (name) values ("Anan"), ("Ikeda"), ("imai"), ("Iwakiri"), ("Shiroshita"), ("Nakagawa"), ("Hashimoto"), ("Munemoto");