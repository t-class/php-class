use salto;

CREATE TABLE iwakiri (
    id int AUTO_INCREMENT NOT NULL PRIMARY KEY,
    name VARCHAR(64) NOT NULL UNIQUE
);

insert into
    iwakiri (name)
values
    ("Anan"),
    ("Ikeda"),
    ("imai"),
    ("Iwakiri"),
    ("Shiroshita"),
    ("Nakagawa"),
    ("Hashimoto"),
    ("Munemoto");