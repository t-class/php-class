use salto;

CREATE TABLE anan
(
    id INT PRIMARY KEY,
    name VARCHAR(10) NOT NULL,
    sex CHAR(1),
    del_flg CHAR(1) NOT NULL DEFAULT 0
);

INSERT INTO anan VALUES
(1, "Anan", "0", "0"),
(2, "Ikeda", "1", "0");