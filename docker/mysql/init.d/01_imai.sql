use salto;

CREATE TABLE if not exists imai (
    id INT unsigned NOT NULL auto_increment primary key, 
    name VARCHAR(20) NOT NULL, 
    delete_flg INT(1) NOT NULL default 0
);

INSERT INTO imai (name) VALUE ("秋田"), ("富山"), ("佐賀"), ("和歌山"), ("香川");