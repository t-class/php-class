use salto;

CREATE TABLE nakano
(
    id INT PRIMARY KEY,
    name VARCHAR(10) NOT NULL,
    sex CHAR(1),
    del_flg CHAR(1) NOT NULL DEFAULT 0
);

INSERT INTO nakano VALUES
(1, "Nakano", "0", "0"),
(2, "Tanaka", "1", "0"),
(3, "Yamada", "0", "0");
