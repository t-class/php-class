include .env

# 初期設定系
init:
	sh scripts/docker-install.sh
	sh scripts/mkdir.sh
	docker-compose up -d

# 起動系
up:
	docker-compose up -d
build:
	docker-compose up -d --build
start:
	docker-compose start
restart:
	docker-compose restart

# コンテナログイン
web:
	docker-compose exec nginx bash
app:
	docker-compose exec php-fpm sh
db:
	docker-compose exec mysql bash
mysql:
	docker-compose exec mysql mysql -u ${MYSQL_USER} -p${MYSQL_PASSWORD} ${MYSQL_DATABASE}

# 監視系
log:
	docker-compose logs
ps:
	docker-compose ps -a

# 停止/削除系
stop:
	docker-compose stop
down:
	docker-compose down
down-all:
	docker-compose down --rmi all --volumes --remove-orphans