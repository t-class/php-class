<?php
namespace Core\DB;

use Core\DB\Parts\DsnEnum;
use Core\Libs\Config;
use Core\Libs\Env;

class Connections {
    private static ?\PDO $_connections = null;
    private string $_dbType = "";
    private array $_data = [];

    private function __construct() {
        $this->_dbType = Env::get("DB_CONNECTION");
        $this->_data = Config::get("database.".$this->_dbType);
        self::_init();
    }

    /**
     * DB接続用インスタンス取得
     * @return \PDO
     */
    public static function get():\PDO {
        if (is_null(self::$_connections))
            new self();
        return self::$_connections;
    }

    /**
     * PDO インスタンス作成
     * @throws PDOException
     */
    private function _init():void {
        $_dsn = sprintf(DsnEnum::get($this->_dbType), $this->_data['host'], $this->_data['port'], $this->_data['database']);
        try {
            self::$_connections = new \PDO($_dsn, $this->_data['user'], $this->_data['password'], $this->_data['options']);
        } catch (\PDOException $ex) {
            echo '<pre>';
            var_dump($ex);
        }
    }

    /**
     * Clone 禁止
     * @throws RuntimeException
     */
    public final function __clone() {
        throw new \RuntimeException('Clone is not allowd again '.get_class($this));
    }

}