<?php
namespace Core\DB\Parts;

enum DsnEnum:string {
    case MYSQL      = "mysql";
    case POSTGRESQL = "pgsql";

    public static function get(string $_dbType):string {
        return match($_dbType) {
            self::MYSQL->value      => "mysql:host=%s;port=%s;dbname=%s;",
            self::POSTGRESQL->value => "pgsql:host=%s;port=%s;dbname=%s;",
        };
    }
}