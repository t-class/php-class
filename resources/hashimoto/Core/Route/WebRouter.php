<?php
namespace Core\Route;

use Core\Libs\Env;

class WebRouter
{

    private const DEFAULT_CONTROLLER = 'Top';
    private const DEFAULT_ACTION = 'index';

    private static string $_type = 'Web';

    public static function createUri():array {
        $_params = [];
        if($_SERVER['REQUEST_URI'])
            $_params = explode(DIRECTORY_SEPARATOR, trim(parse_url($_SERVER['REQUEST_URI'],PHP_URL_PATH), DIRECTORY_SEPARATOR));

        $_controller = self::DEFAULT_CONTROLLER;
        $_action     = self::DEFAULT_ACTION;
        if(!current($_params))
            return [$_controller, $_action];

        if(!next($_params)) {
            $_controller = ucfirst(reset($_params));
            return [$_controller, $_action];
        }

        $_controller = ucfirst(reset($_params));
        $_action     = next($_params);
        return [$_controller, $_action];
    }
}
