<?php
namespace Core;

use Core\Route\WebRouter;

require_once __DIR__."/Base.php";

class Dispatcher extends Base {
    private static ?Dispatcher $_instance = null;

    public function __construct() {
        parent::__construct();
    }

    public static function dispatch():void {
        if(is_null(self::$_instance)) self::$_instance = new self();

        // Routing
        list($_controller, $_action) = WebRouter::createUri();        
        $_controllerInstance = self::_getControllerInstance($_controller);
        if(!method_exists($_controllerInstance, $_action))
            exit("指定されたメソッドがありません：".$_action);
        
        $_controllerInstance->$_action();
    }

    private static function _getControllerInstance(string $_controller):object {
        $_className = sprintf("%sController", $_controller);
        if(!file_exists(sprintf("%s/app/Controllers/%s.php", dirname(__FILE__, 2), $_className)))
            exit("指定されたファイルがありません：".$_className);

        require_once sprintf("%s/app/Controllers/%s.php", dirname(__FILE__, 2), $_className);
        $_className = "app\Controllers\\".$_className;
        return new $_className();
    }

    /**
     * Clone 禁止
     * @throws RuntimeException
     */
    public final function __clone() {
        throw new \RuntimeException('Clone is not allowd again '.get_class($this));
    }
}