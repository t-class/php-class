<?php
namespace Core;

abstract class Base {
    private string $_rootPath = "";

    public function __construct() {
        $this->_setHandler();
        $this->_setRootPath($this->_rootPath);
        spl_autoload_register([$this, '_baseAutoloader']);
    }

    private function _setHandler():void {
        return;
    }

    private function _setRootPath(string $_rootPath = ""):void {
        if(!$_rootPath) $this->_rootPath = dirname(__DIR__, 1);
        return;
    }

    private function _baseAutoloader(string $_className):void {
        $_className = ltrim($_className, '\\');
        list($_isReader, $_filePath) = $this->doCheckReaderbleFile($_className);
        if(!$_isReader)
            throw new \ErrorException("指定されたファイルが存在しません: ".$_filePath);
        
        require_once $_filePath;
        return;
    }

    protected function doCheckReaderbleFile(string $_className):array {
        $_list = [$this->_rootPath, DIRECTORY_SEPARATOR, str_replace("\\", DIRECTORY_SEPARATOR, $_className)];
        $_filePath = vsprintf(sprintf("%s.php", str_repeat("%s", count($_list))), $_list);
        return [is_readable($_filePath), $_filePath];
    }
}