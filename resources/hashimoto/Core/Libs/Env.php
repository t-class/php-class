<?php
namespace Core\Libs;

use Exception;

class Env {
    private static string $_appEnv   = "";
    private static array  $_envList  = [];
    private static ?Env   $_instance = null;

    private function __construct() {
        self::$_appEnv = self::getAppEnv();
        self::setEnvArray();
    }
    
    public static function get(string $_key, mixed $_default = null):mixed {
        if(empty(self::$_envList)) self::$_instance = new self();
        if(!array_key_exists($_key, self::$_envList)) return $_default;
        return self::$_envList[$_key];
    }

    private static function getAppEnv():string {
        if(!self::$_appEnv) self::setAppEnv();
        return self::$_appEnv;
    }

    private static function setAppEnv():void {
        if($_SERVER['APP_ENV']) self::$_appEnv = $_SERVER['APP_ENV'];
    }

    private static function setEnvArray():void {
        $_fileName = sprintf("%s%s.env%s%s", dirname(__FILE__, 3), DIRECTORY_SEPARATOR, DIRECTORY_SEPARATOR, self::$_appEnv);
        if(!is_file($_fileName)) throw new Exception();

        $_file = file($_fileName, FILE_SKIP_EMPTY_LINES);
        foreach($_file as $row) {
            // コメントアウトは読み込まない
            if(preg_match('/^PHP_INI_|^\#|^\n/', $row)) continue;
            $_data = explode("=", trim($row), 2);
            self::$_envList[current($_data)] = next($_data);
        }
    }
}