<?php
namespace Core\Libs;

class Config {    
    /**
     * アプリ定数の取得
     * @param string $_param path.fileName.key
     * @param mixed $_default
     * @return mixed
     */
    public static function get(string $_param, mixed $_default = null):mixed {
        $_confPath = dirname(__FILE__, 3).DIRECTORY_SEPARATOR."config";
        if (!is_dir($_confPath)) return $_default;

        $_arrParam = array_map('trim', explode(".", $_param));

        $_dir = $_confPath.DIRECTORY_SEPARATOR.current($_arrParam);
        while (is_dir($_dir)) {
            $_dir .= DIRECTORY_SEPARATOR.next($_arrParam);
        }

        $_file = $_dir.".php";
        $_data =  is_file($_file) ? require($_file) : $_default;

        while (next($_arrParam)) {
            $_data = array_key_exists(current($_arrParam), $_data) ? $_data[current($_arrParam)] : $_default;
        }
        return $_data;
    }
}