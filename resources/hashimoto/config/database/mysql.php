<?php
use Core\Libs\Env;

return [
    'driver'    => "mysql",
    'host'      => Env::get("DB_HOST", "localhost"),
    'port'      => Env::get("DB_PORT", "3306"),
    'database'  => Env::get("DB_NAME", "3306"),
    'user'      => Env::get("DB_USER", "user"),
    'password'  => Env::get("DB_PASS", ""),
    'charset'   => "utf8mb4",
    'collation' => 'utf8mb4_unicode_ci',
    'write'     => [],
    'read'      => [],
    'options'   => [
        \PDO::ATTR_ERRMODE            => \PDO::ERRMODE_EXCEPTION,
        \PDO::ATTR_DEFAULT_FETCH_MODE => \PDO::FETCH_ASSOC,
        \PDO::ATTR_EMULATE_PREPARES   => false
    ],
];