<?php
namespace app\Controllers;

class TopController {
    public function index() {
        echo "Called TopController";
    }

    public function hoge() {
        echo "Called hoge method by TopController";
    }
}