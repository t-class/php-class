<?php

namespace Core\Libs;

use Exception;
use Utils\RegexPatterns;
use Utils\FileManager;
use Utils\ConfigManager;

class Env
{
	private static string $env_pass = '/var/www/nakano/.env/';
	private const ERR_KEY_NOT_EXISTS = 'ファイルに対象のキーが存在しません';

	/**
	 * 環境設定ファイルから引数をキーとして値を取得して返す
	 *
	 * @param string $arg
	 * @return string
	 */
	public static function get(string $arg): string
	{
		$envFileLines = self::_getEnvFile();
		foreach ($envFileLines as $line) {
			if (trim($line) === "" || 0 === strpos(trim($line)[0], '#')) {
				continue;
			}
			$envSettingArr = preg_split(RegexPatterns::SPLIT_KEY_VALUE, $line, 2);
			$envSettingArr = array_map('trim', $envSettingArr);
			if (current($envSettingArr) === $arg) {
				return next($envSettingArr);
			}
		}
		throw new Exception(self::ERR_KEY_NOT_EXISTS);
	}

	/**
	 * 定義ファイルに記載のPHP設定値を設定する
	 *
	 * @return void
	 */
	public static function set(): void
	{
		$envFileLines = self::_getEnvFile();
		foreach ($envFileLines as $line) {
			if (trim($line) === "" || 0 === strpos(trim($line)[0], '#')) {
				continue;
			}
			$phpSettingArr = preg_split(RegexPatterns::SPLIT_KEY_VALUE, $line, 2);
			$phpSettingArr = array_map('trim', $phpSettingArr);
			if (0 === strpos($phpSettingArr[0], 'PHP_INI_')) {
				$option = mb_strtolower(str_replace('PHP_INI_', '', current($phpSettingArr)));
				$value = next($phpSettingArr);
				ConfigManager::ini_set($option, $value);
			}
		}
	}

	/**
	 * 環境設定ファイルの取得
	 *
	 * @return array
	 */
	private static function _getEnvFile(): array
	{
		$app_env = $_SERVER['APP_ENV'];
		try {
			return FileManager::file(self::$env_pass . $app_env);
		} catch (Exception $e) {
			throw $e;
		}
	}
}
