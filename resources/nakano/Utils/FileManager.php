<?php

namespace Utils;

use Exception;

/**
 * ファイル関連処理の例外ハンドリングクラス
 */
class FileManager
{
	private const ERR_FILE_READ_FAILURE = 'ファイルの読み込みに失敗しました';
	private const ERR_FILE_NOT_EXISTS = 'ファイルが存在しないか読み取りの許可がありません';

	/**
	 * file関数の例外ハンドリング
	 *
	 * @param string $filename
	 * @return array
	 * @throws Exception
	 */
	public static function file(string $filename): array
	{
		$fileContent = file($filename);
		if ($fileContent === false) {
			throw new Exception(self::ERR_FILE_READ_FAILURE);
		}
		return $fileContent;
	}

	public static function is_readable(string $filename): void
	{
		$condition = is_readable($filename);
		if (!$condition) {
			throw new  Exception(sprintf('%s ファイル名：%s ',self::ERR_FILE_NOT_EXISTS,$filename));
		}
	}
}
