<?php
require_once(realpath(dirname(__DIR__)) . '/Core/Dispatcher.php');

use Core\Dispatcher;
use Core\DB\DBconnector;
use Core\Libs\Config;
use Core\Libs\Env;

// phpinfo();

// Lesson5
Dispatcher::getInstance();

// Lesson2
$conn = DBconnector::getConnection();
$sql = 'SELECT * FROM nakano';
foreach ($conn->query($sql) as $row) {
	print $row['id'] . "：";
	print $row['name'] . "：";
	print $row['sex'] . "<br>";
}

// Lesson3
// echo "<hr>";
// print "パターン1：hoge.huga"."<br>";
// print "hogeディレクトリにhuga.phpが存在する(値の全取得)"."<br>";
// var_dump(Config::get('hoge.huga'));

// echo "<hr>";
// print "パターン2：hoge.huga.tata"."<br>";
// print "hogeディレクトリにhuga.phpが存在し、キー：tataを検索して取得"."<br>";
// var_dump(Config::get('hoge.huga.tata'));

// echo "<hr>";
// print "パターン2_1：hoge.huga.tata, dainihikisuu"."<br>";
// print "hogeディレクトリにhuga.phpが存在し、キー：tataが存在する"."<br>";
// var_dump(Config::get('hoge.huga.tata', 'dainihikisuu'));

// echo "<hr>";
// print "パターン2_2：hoge.huga.tatatata, dainihikisuu"."<br>";
// print "hogeディレクトリにhuga.phpが存在し、キー：tatatataが存在しない(第二引数が帰る)"."<br>";
// var_dump(Config::get('hoge.huga.tatatata', 'dainihikisuu'));

// echo "<hr>";
// print "パターン3：hoge.huga.hoge.fuga"."<br>";
// print "hogeディレクトリにhuga.phpが存在し、キー：hogeの値からさらにキー：fugaを検索して取得"."<br>";
// var_dump(Config::get('hoge.huga.hoge.fuga'));

// echo "<hr>";
// print "パターン4：hoge.huga.hoge.fuga.hage"."<br>";
// print "パターン3と共通だが最後に存在しないキー：hageを検索し、エラー"."<br>";
// var_dump(Config::get('hoge.huga.hoge.fuga.hage'));

// echo "<hr>";
// print "パターン5：huga.hige.uga"."<br>";
// print "存在しないファイルhigeを検索し、エラー"."<br>";
// var_dump(Config::get('huga.hige.uga'));

// echo "<hr>";
// print "パターン6：fuga.fuga"."<br>";
// print "同じ階層にファイル名と同名のディレクトリが存在する場合、ファイルの方を対象に検索"."<br>";
// var_dump(Config::get('fuga.fuga'));

// echo "<hr>";
// print "パターン6_1：fuga.hige"."<br>";
// print "6の派生、ファイルを検索するがキーが存在しない"."<br>";
// var_dump(Config::get('fuga.hige'));

// echo "<hr>";
// print "パターン6_2：fuga.hige, dainihikisuu"."<br>";
// print "6_1の派生、第二引数を渡している"."<br>";
// var_dump(Config::get('fuga.hige', 'dainihikisuu'));

// echo "<hr>";
// print "パターン7：uga.hige.hoge"."<br>";
// print "2階層以上ディレクトリを検索する(uga/hige/hoge.php)"."<br>";
// var_dump(Config::get('uga.hige.hoge'));

// echo "<hr>";

// Lesson4
echo "<hr>";
echo "パラメータ；MYSQL_PORTの取得"."<br>";
try {
	$env = Env::get("MYSQL_PORT");
	print $env . "<hr>";
} catch (Exception $e) {
	print "エラーメッセージ：" . $e->getMessage();
}
echo "<hr>";
echo "パラメータ；PGSQL_USERの取得"."<br>";
try {
	$env = Env::get("PGSQL_USER");
	print $env . "<hr>";
} catch (Exception $e) {
	print "エラーメッセージ：" . $e->getMessage();
}
echo "<hr>";
echo "PHPの初期値" . "<br>";
echo "mbstring.language" . ":";
echo ini_get("mbstring.language") . "<br>";
echo "display_errors" . ":";
echo ini_get("display_errors") . "<br>";

echo "<hr>";
echo "PHPの設定変更実行後(Env::set()の実行)" . "<br>";
Env::set();
echo "mbstring.language" . ":";
echo ini_get("mbstring.language") . "<br>";
echo "display_errors" . ":";
echo ini_get("display_errors") . "<br>";
