<?php

namespace Core\Libs;

class Config
{
    private static string $confDir;

    public static function get(string $param, mixed $default = null):mixed {
        self::_setConfDir();
        $arrParams = explode(".", $param);
        if(!$file = self::_getFile(self::$confDir, $arrParams, $default))
            return $default;
        $conf = require $file;
        if(!$ret = self::_isUniqueFile($file, $arrParams, $default))
            return $default;
        if($ret === "no_key")
            return $conf;
        return self::_getVal($conf, self::_getKeys($arrParams), $default);
    }

    private static function _setConfDir():void {
        if(!isset(self::$confDir)) 
        self::_isConfDir(self::$confDir = realpath(dirname(__DIR__, 2). DIRECTORY_SEPARATOR. 'config'));
    }

    private static function _isConfDir(string $confDir):void {
        if(!is_dir($confDir))
            throw new \RuntimeException(sprintf("Config directory was not found in %s", $confDir));
    }

    private static function _getFile(string $confDir, array &$arrParams, mixed $default):mixed {
        $file = $confDir.DIRECTORY_SEPARATOR.current($arrParams);
        while(is_dir($file))
            $file .= DIRECTORY_SEPARATOR.next($arrParams);
        if(!file_exists($file .= ".php")) 
            return isset($default) ? false : throw new \RuntimeException("File was not found");
        return $file;
    }

    private static function _isUniqueFile(string $file, array &$arrParams, mixed $default):string|bool {
        $parentDirFile = rtrim($file, DIRECTORY_SEPARATOR.current($arrParams).'.php').'.php';
        if(!next($arrParams)) {
            if(file_exists($parentDirFile)) {
                $parentDirConf = require $parentDirFile;
                if(array_key_exists(end($arrParams), $parentDirConf))
                    return isset($default) ? false : throw new \RuntimeException("Confusing path");
            }
            return "no_key";
        }
        return true;
    }

    private static function _getKeys(array &$arrParams):array {
        $arrKeys[] = current($arrParams);
        while(next($arrParams))
            $arrKeys[] = current($arrParams);
        return $arrKeys;
    }

    private static function _getVal(array $conf, array $arrKeys, mixed $default):mixed {
        foreach($arrKeys as $v) {
            if(!is_array($conf))
                throw new \RuntimeException("Much parameters");
            if(!array_key_exists($v, $conf)) 
                return isset($default) ? $default : throw new \RuntimeException("Key was not found");
            $conf = $conf[$v];
        }
        return $conf;
    }
}