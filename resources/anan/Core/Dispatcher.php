<?php

namespace Core;

require_once __DIR__ . "/Base.php";
use Core\Base;
use Core\Environment\Env;

class Dispatcher extends Base
{
    public static ?Dispatcher $instance = null;

    /**
     * クローン禁止
     * @throws \ErrorException
     */
    public function __clone() {
        throw new \ErrorException("Clone is not allowed against" . $this);
    }

    private function __construct() {
        parent::__construct();
    }

    public static function dispatch():void {
        if(is_null(self::$instance))
            self::$instance = new self();
        var_dump(Env::get("MYSQL_PORT"));
    }
}