<?php

class DbConnector
{
    // dsn(data source name)
    private string $_dsn = "%s:host=%s;dbname=%s;charset=%s;";
    private static ?\PDO $_con = null;
    private const DB_DRIVER = "mysql";
    private const DB_HOST = "mysql";
    private const DB_SCHEMA = "salto";
    private const DB_ENCODE = "UTF8";
    private const DB_USER = "salto";
    private const DB_PASSWORD = "rcbMmj95cqde";

    /**
     * クローン禁止
     * @throws ErrorException
     */
    public final function __clone() {
        throw new \ErrorException("Clone is restricted against ". get_class($this));
    }

    private function __construct() {
        $this->_init();
    }

    private function _init():void {
        $dsn = sprintf($this->_dsn, self::DB_DRIVER, self::DB_HOST, self::DB_SCHEMA, self::DB_ENCODE);
        $pdoOption = [
            \PDO::ATTR_ERRMODE            => \PDO::ERRMODE_EXCEPTION,
            \PDO::ATTR_DEFAULT_FETCH_MODE => \PDO::FETCH_ASSOC,
            \PDO::ATTR_EMULATE_PREPARES   => false
        ];
        try {
            self::$_con = new \PDO($dsn, self::DB_USER, self::DB_PASSWORD, $pdoOption);
        }catch(\PODException $e) {
            echo $e->getMessage();
        }
    }

    public static function getInstance():\PDO {
        if(is_null(self::$_con)) {
            new self();
        }
        return self::$_con;
    }
}