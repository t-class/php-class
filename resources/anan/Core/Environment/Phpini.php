<?php 

require_once realpath(__DIR__. '/Traits/EnvTrait.php');

class Phpini
{
    use EnvTrait;

    public static function set(string $option, string|int|float|bool|null $value):void {
        $option = mb_strtolower($option);
        if(!ini_set($option, $value))
            throw new \RuntimeException('ini_set failed');
    }

    /**
     * set multiple configurations
     * @param array $arrOption [string $option1 => mixed $value1, $option2 => $value2, ...]
     */
    public static function setAll(array $arrOptions):void {
        foreach($arrOptions as $k => $v) {
            if(!ini_set($k, $v))
                throw new RuntimeException('ini_set failed');
        }
    }

    public static function get(string $option):string {
        $option = mb_strtolower($option);
        $ret = ini_get($option);
        return $ret ? $ret : throw new \RuntimeException('ini_get failed');
    }
 
    public static function setFromEnv():void {
        self::setAll(self::_loadFile('phpini'));
    }
}