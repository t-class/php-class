<?php
namespace Core\Environment\Traits;

Trait EnvTrait
{
    /**
     * @TODO エラーハンドリング
     */
    private static function _getFile():string {
        $appEnv = $_SERVER['APP_ENV'];
        return realpath(sprintf('%s/.env/%s', dirname(__DIR__, 3), $appEnv));
    }

    /**
     * Make an associative array from env file
     * [env file] line x : $k = $v ====> [$k => $v]
     * @param string $path, string $option = "env" if 
     * if $option === "phpini", get configurations only that start with "PHP_INI_"
     */
    private static function _loadFile(string|null $option = null):array {
        if(!$fp = fopen(self::_getFile(), 'r'))
            throw new \RuntimeException("Env file was not found");
        while($line = fgets($fp)) {
            if(str_starts_with($line, '#') || !isset($line)) continue;
            // read only PHP settings
            if($option === 'phpini') {
                if(!str_starts_with($line, 'PHP_INI_')) continue;
                $params = explode('=', $line, 2);
                $envContents[mb_strtolower(str_replace('PHP_INI_', '', trim($params[0])))] = trim($params[1]);
                continue;
            }
            $params = explode('=', $line, 2);
            if (!trim($params[0])) continue;
            $envContents[trim($params[0])] = trim($params[1]);
        }
        fclose($fp);
        return $envContents;
    }
}