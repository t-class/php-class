<?php

namespace Core\Environment;

use Core\Environment\Traits\EnvTrait;

class Env
{
    use EnvTrait;

    public static function get(string $key):mixed {
        $envContents = self::_loadFile();
        return $envContents[mb_strtoupper($key)];
    }
}