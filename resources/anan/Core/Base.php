<?php

namespace Core;

class Base
{
    private string $_rootPath = "";

    public function __construct() {
        spl_autoload_register([$this, 'myAutoloader']);
    }

    private function myAutoloader(string $className):void {
        $this->_setRootPath();
        $this->_loadFile($className);
    }

    private function _setRootPath():void {
        if(!$this->_rootPath)
            $this->_rootPath = dirname(__DIR__);
    }

    private function _loadFile(string $className):void {
        $file = sprintf("%s%s%s.php", $this->_rootPath, DIRECTORY_SEPARATOR, str_replace("\\", DIRECTORY_SEPARATOR, $className));
        $this->_checkFileExists($file);
        require_once $file;
    }

    private function _checkFileExists(string $file):void {
        if(!file_exists($file)) {
            throw new \ErrorException(sprintf("file %s could not be found", $file));
        }
    }
}