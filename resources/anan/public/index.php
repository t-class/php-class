<?php

// require_once realpath(dirname(__DIR__). "/Core/DB/DbConnector.php");
// require_once realpath(dirname(__DIR__). "/Core/Libs/Config.php");
// require_once realpath(dirname(__DIR__). "/Core/Environment/Env.php");
// require_once realpath(dirname(__DIR__). "/Core/Environment/Phpini.php");
require_once realpath (dirname(__DIR__). "/Core/Dispatcher.php");
use Core\Dispatcher;


// echo "<pre>";

// lesson2
// echo dirname(__DIR__);
// try {
//     $con = \DbConnector::getInstance();
//     $con->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
//     $sql = "SELECT * FROM anan";
//     var_dump($con->query($sql)->fetchall(\PDO::FETCH_ASSOC));
// } catch(\PDOException $e) {
//     var_dump($e->__toString());
//     exit();
// }


// lesson3
// try {
//     echo "/config/piyopiyo は存在しないのでデフォルト値がなければエラー \n";
//     var_dump(Config::get("piyopiyo", "default"));

//     echo "/config/mysql.php の値をすべて取得 \n";
//     var_dump(Config::get("mysql"));

//     echo "/config/mysql.php のキー 'password' の値を取得 \n";
//     var_dump(Config::get("mysql.password"));

//     echo "/config/mysql.php にキー 'ss' が存在しないので デフォルト値を返す \n";
//     var_dump(Config::get("mysql.ss", "default"));

//     echo "/config/hoge.php 内の hoge 配列の中の キー 'fuga' の値を取得 \n";
//     var_dump(Config::get("hoge.hoge.fuga", "default"));

//     echo "/config/fuga 中のファイルの指定がないのでデフォルト値を返す \n";
//     var_dump(Config::get("fuga","default"));

//     echo "/config/fuga/piyo.phpの値をすべて取得 \n";
//     var_dump(Config::get("fuga.piyo"));

//     echo "/config/fuga/piyo.php のキー'tata'の値を返す \n";
//     var_dump(Config::get("fuga.piyo.tata"));

//     // echo "余分な引数が存在しているのでエラー \n";
//     // var_dump(Config::get("fuga.piyo.tata.ss", "default"));

//     echo "/config/foo.php のキー 'bar' と /config/foo/bar.php \n";
//     var_dump(Config::get("foo.bar", "default"));

//     var_dump(Config::get("hoge.fuga.piyo.hoge.fuga"));

// } catch(\RuntimeException $e) {
//     var_dump($e->__toString());
// }

// lesson4
// var_dump(Env::get('MYSQL_USER'));
// var_dump(Env::get('MYSQL_DATABASE'));

Dispatcher::dispatch();