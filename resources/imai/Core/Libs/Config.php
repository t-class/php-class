<?php

class Config
{

    private static string $_confPath = "";

    // get（値,　default）
    public static function get(string $route, mixed $default = null) {
        self::$_confPath = dirname(__DIR__, 2) . DIRECTORY_SEPARATOR . 'Config' . DIRECTORY_SEPARATOR;
        $convertRouteValue = self::setConvertRouteValue($route);
        $directoryPath = self::$_confPath;
        $obtainedValue = self::valueSortOut($directoryPath, $convertRouteValue);
        return $obtainedValue;
    }

    // $routeの成形用メソッド
    private static function setConvertRouteValue(string $route) : array {
        $convertRouteValue = preg_split('/\./', $route, -1, PREG_SPLIT_NO_EMPTY);

        if($convertRouteValue){
            return $convertRouteValue;
        }else{
            echo "NOT ROUTE VALUE";
            exit;
        }
    }

    // if文ごとにメソッドを分けた方がいいかも
    private static function valueSortOut(string $directoryPath, array $convertRouteValue) : string | array {
        // ディレクトリがあったときディレクトリ名を取得
        foreach ($convertRouteValue as $value) {
            // Config下に一致するディレクトリがあった場合
            if(!empty(glob($directoryPath . $value))){
                $directoryPath .= $value;
                continue;
            }
            // ファイルの中身を変数に代入
            if(file_exists($directoryPath . DIRECTORY_SEPARATOR . $value . ".php")){
                $targetFilePath = $directoryPath . DIRECTORY_SEPARATOR . $value . ".php";
                $targetFileData = require_once($targetFilePath);
                continue;
            }
            // 一致するキーがあった場合
            if(array_key_exists($value, $targetFileData)){
                $targetValue = $targetFileData[$value];
                continue;
            }
            // 連想配列の場合
            if(!is_null($value)){
                if(array_key_exists($value, $targetValue)){
                    $targetValue = $targetValue[$value];
                    continue;
                }
                return $targetValue;
            }
        }
        if(isset($targetValue)){
            return $targetValue;
        }
        return $targetFileData;
    }
}