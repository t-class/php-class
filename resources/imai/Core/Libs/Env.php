<?php

class Env 
{
    public static function get(string $targetKey) {

        // ファイルの取得
        $targetFile = self::getTargetFile();

        // 値の取得
        $targetValue = self::getTargetValue($targetFile, $targetKey);
        if(is_null($targetValue)) {
            echo "NOT VALUE AT " . $targetKey;
            exit;
        }
        
        // $_SERVER['APP_ENV']がnullだったら文字列を返す
        if(is_null($_SERVER['APP_ENV'])) {
            echo "NOT APP_ENV INFOMATION";
            exit;
        }
        return $targetValue;
    }

    private static function getTargetFile() {

        // $_SERVER['APP_ENV']があったらそのファイルを取得
        if(isset($_SERVER['APP_ENV'])) {
            // .envフォルダ内の$_SERVER['APP_ENV']ファイルへのパスを取得
            $targetFile = realpath(dirname(__DIR__, 2) . "/.env/" . $_SERVER['APP_ENV']);
            return $targetFile;
        }else{
            echo "NOT FOUND FILE";
            exit;
        }
    }

    private static function getTargetValue(string $targetFile, string $targetKey) {

        if(isset($targetKey)) {

            $targetData = fopen($targetFile, "r");

            if($targetData) {
                while ($targetString = fgets($targetData)) {

                    // コメントアウトのものを除外
                    if(substr($targetString, 0, 1) == "#") {
                        $targetString = null;
                    }
                    $nonBlankData = preg_replace("/(\s+|\S+|)/", "", $targetString);
                    // 初期化
                    $Data = array();
                    $Data = explode('=', $nonBlankData, 2);
                    echo "<br>";
                    if ($Data[0] == $targetKey) {
                        return $Data[1];
                    }
                }
            }

            fclose($targetData);

        }
    }

        

}

?>