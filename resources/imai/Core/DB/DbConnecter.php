<?php
class MyPdo
{

    private static array $_dsn = [];
    private static ?\PDO $_connection = null;

    private function __construct() {

        try{

            self::$_dsn = [
                'user' => Env::get('MYSQL_USER', ''),
                'password' => Env::get('MYSQL_PASSWORD', 'user'),
                'content' => Env::get('MYSQL_CONTENT', '')
            ];

            self::$_connection = new PDO(self::$_dsn['content'], self::$_dsn['user'], self::$_dsn['password']);
            self::$_connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        }catch(\PDOException $e){
            echo "エラー：" . $e->getMessage();
        }  
    }

    /**
     * Undocumented function
     *
     * @return PDO
     */
    public static function getInstance(): PDO {
        if (is_null(self::$_connection))
            new self();
        return self::$_connection;
    }

    // クローン禁止
    public final function __clone() {
        throw new Exception("this instance is singleton class.");
    }
}
?>