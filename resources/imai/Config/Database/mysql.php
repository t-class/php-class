<?php

return [
    'driver'    =>"mysql",
    'host'      =>"mysql",
    'port'      =>3306,
    'database'  =>"salto",
    'user'      =>Env::get("MYSQL_USER", "user"),
    'password'  =>Env::get("MYSQL_PASSWORD", ""),
    'charset'   =>"utf8mb4",
    'colection' =>'utf8mb4_unicode_ci',
    'write'     =>[],
    'read'      =>[],
    // 'option'    =>[
    //     \PDO::ATTR_ERRMODE              => \PDO::ERRMODE_EXCEPTION,
    //     \PDO::ATTR_DEFAULT_FETCH_MODE   => \PDO::FETCH_ASSOC,
    //     \PDO::ATTR_EMULATE              => false
    // ],
];