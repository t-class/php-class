<?php
require_once('../Core/DB/DbConnecter.php');

class MainModel{

    private object $_connection;

    public function __construct()
    {
        $this->_connection = MyPdo::getInstance();
    }
    
    // クエリを実行してDBから全ての値を取得
    public function getAllData(){

        $sql = "SELECT * FROM imai ORDER BY id";

        try {
            $stmt = $this->_connection->query($sql);
            return $stmt->fetchAll(PDO::FETCH_ASSOC);
        } catch (\PDOException $e) {
            header('Location: http://' . $_SERVER['HTTP_HOST'] . '/main');
            exit();
        }
    }
}
?>