<?php
require_once('../app/Model/MainModel.php');
require_once('../Core/Libs/Env.php');
require_once('../Core/Libs/Config.php');

class MainController{

    private array $data;
    private mixed $hoge;
    private object $mainModel;
    private object $env;
    private object $main;

    public function __construct(){

        $this->init();
        $this->getAllData();
        // $this->index();

    }

    public function init(){

        $this->data = array();
        $this->hoge = array();
        $this->mainModel = new MainModel();
        return;

    }

    // Modelからデータの取得
    public function getAllData(){
        
        $this->data = $this->mainModel->getAllData();
        // Config::setConfigDirectory(realpath("../") . '/Config');
        // 理想はこう？
        // $thid->huga = /var/www/imai/Controller/Config/hoge[huga];
        $this->hoge = Config::get('Database.mysql.password', $default = null);
        var_dump($this->hoge);
        require_once('../app/View/main.php');
        return;
    }
}

?>