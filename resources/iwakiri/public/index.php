<?php
// error_reporting(0);
try {
  require_once(dirname(__DIR__, 1) . "/Core/Lib/Dispatcher.php");
  Dispatcher::getInstance();
  Log::getInstance();
  require_once(dirname(__DIR__, 1) . "/Base/set_error_handler.php");
} catch (Throwable $e) {
  var_dump($e);
  exit();
}
