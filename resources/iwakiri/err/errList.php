<?php
return [
  0 => [
    0 => '000@Nothing::エラーコードがありません。',
    1 => '001@Nothing File::ファイルがありません。',
    4 => '004@No environmental information available::環境情報がありません。',
    5 => '005@There is no setting value::設定値がありません。',
    6 => 'No environment selected::環境が選択されていません。<br>
            <hr>
            etc/nginx/conf.d/default.confファイルに以下の一行を追加してください。<br>
            環境名は、prod,stg,localのいずれかを追加してください。
            <hr>
            <b>fastcgi_param APP_ENV 環境名;</b>
            <hr>
            location ~ \.php$ {
                <p>　　fastcgi_pass php-fpm:????;</p>
                <p>　　fastcgi_index index.php;</p>
                <p>　　fastcgi_param SCRIPT_FILENAME $realpath_root$fastcgi_script_name;</p>
                <p>　　<b>fastcgi_param APP_ENV local;</b></p>
            include fastcgi_params;}',
    // 7 => '007@ Value is empty::値が空っぽです。<hr><b>Request :: </b>',
    8 => '008@ Value name not found::システムか値の名前が見つかりません。<hr><b>Request :: </b>',
    // 9 => '009@ The following systems are not supported::以下のシステムは対応していません。<hr>',
    10 => "010@Unable to create directory::ディレクトリが作成できません",
    20 => "020@BaseAutoLoad cannot be loaded::BaseAutoLoadが読み込めません",
    21 => "021@Only one BaseAutoLoad can be created::BaseAutoLoadは1つしか作れません",
    23 => "023@I searched deep in the directory but couldn't find it::ディレクトリの奥底まで探したけどありませんでした。",
    24 => "024@Dispatcher does not have the following features::Dispatcherに以下の機能はありません。",
    28 => "028@Could not write to log for some reason::何らかの理由でログに書き込めませんでした",
  ],
  4 => [
    4 => '404@Not Found::ページが見つかりません。'
  ],
  7 => [
    0 => '700@unknown::DBのどこかで問題が発生しています。',
    1 => '701@server_down::DBサーバーが落ちています。',
    2 => '702@unknown_connection::接続情報が違います。',
    3 => '703@Problem_with_getting::情報取得で問題が発生しました。',
    4 => '704@Problem_with_insert::情報挿入で問題が発生しました。',
    5 => '705@MyPDO is unique::MyPDOは唯一無二です。',
    77 => '777@DB is fine::DBは正常です。'
  ]
];
