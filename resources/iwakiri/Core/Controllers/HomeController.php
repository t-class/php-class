<?php
class HomeController
{
  private ?IwakiriModel $_iwakiriModel = null;
  private ?Session $_session = null;
  private ?View $_view = null;
  private ?Request $_request = null;
  public function __construct(Request $request)
  {
    $this->_iwakiriModel = new IwakiriModel();
    $this->_session = new Session();
    $this->_request = $request;
    $this->_view = new View();
  }

  /**
   * Viewを呼び出して画面表示
   * @return void
   */
  public function index(): void
  {
    session_start();

    $column = "*";
    $result = $this->_iwakiriModel->getName($column);

    $this->_view->setValue("study_member", $result);
    $this->_view->output($this->_request->moveKey);

  }
}
