<?php
class DocumentController
{
  private ?View $_view = null;
  private ?Request $_request = null;
  public function __construct(Request $request)
  {
    $this->_request = $request;
    $this->_view = new View();
  }

  public function index(): void
  {
    var_dump(LogLevel::INFO);
    echo "<hr>";
    var_dump(LogLevel::INFO->logLevel(LogLevel::INFO));
    // $this->testEnum("<hr>Test");




    //変数に入れてviewで表示予定

    if ($this->_request->getKeyExistCheck('class_name') && class_exists($this->_request->get('class_name'), false)) {
      var_dump($this->methodList($this->_request->get('class_name')));
      echo "<hr>";
      var_dump($this->varList($this->_request->get('class_name')));
      echo "<hr>";
      var_dump($this->getCommentOut($this->_request->get('class_name')));
    };
    $this->_view->output($this->_request->moveKey);
  }
  private function testEnum(LogLevel $value)
  {
    var_dump($value);
  }

  //メソッド一覧取得
  public function methodList(string $className): array
  {
    return get_class_methods(ucfirst($className)) ? get_class_methods(ucfirst($className)) : [];
  }
  //変数一覧取得
  public function varList(string $className): array
  {
    return get_class_vars(ucfirst($className)) ? get_class_vars(ucfirst($className)) : [];
  }
  //コメントアウト取得
  public function getCommentOut(string $className): string
  {
    //ReflectionMethodにコメントアウト取得機能が無い
    // return (new ReflectionMethod($className))->getDocComment();
    //クラス名に1番近いコメントアウトを取得
    $comment = (new ReflectionClass($className))->getDocComment();
    return $comment ? $comment : "";
  }
}
