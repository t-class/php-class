<?php
class RequestController
{
  private ?Session $_session = null;
  private ?Request $_request = null;
  private ?View $_view = null;
  public function __construct(Request $request)
  {
    $this->_session = new Session();
    $this->_request = $request;
    $this->_view = new View();
  }
  public function index(): void
  {
    session_start();
    $this->_view->output($this->_request->moveKey);
  }
}
