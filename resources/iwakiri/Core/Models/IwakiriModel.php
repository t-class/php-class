<?php
class IwakiriModel extends BaseDbModel
{
    /**
     * @@テーブル名べた書き
     * @@カラム名とオプション複数対応したい
     *
     * @param string $column
     * @return array|string
     */
    public function getName(string $column): array|string
    {
        $sql = "SELECT $column FROM iwakiri";
        return $this->select($sql);
    }
}
