<!DOCTYPE html>
<html lang="ja">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <!-- <link rel="stylesheet" href="../Core/Views/css/home.css" type="text/html"> -->
  <title>php-class</title>
</head>

<body>
  <h1>PHP-CLASS</h1>
  <ul>
    <li style="list-style: none;">
      <a href="/request/index">Request</a>
      <a href="/document/index">DOC</a>
    </li>
  </ul>

  <form method="GET" action="/home/index">
    <p>お名前：
      <input type="text" name="name">
      <input type="submit" value="GET送信">
    </p>
  </form>
  <form method="POST" action="/home/index">
    <p>お名前：
      <input type="text" name="postName">
      <input type="submit" value="POST送信">
    </p>
  </form>

  <?
  var_dump($this->getValue('study_member'));
  ?>

</body>

</html>