<!DOCTYPE html>
<html lang="ja">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Document</title>
</head>

<body>
  <h1>PHP-CLASS</h1>
  <ul>
    <li style="list-style: none;">
      <a href="/home/index">HOME</a>
      <a href="/request/index">Request</a>
    </li>
  </ul>
  <h1>document</h1>
  <form method="GET" action="/document/index">
    <input type="hidden" name="class_name" value="Dispatcher">
    <input type="submit" value="Dispatcher">
  </form>
  <form method="GET" action="/document/index">
    <input type="hidden" name="class_name" value="autoLoad">
    <input type="submit" value="autoLoad">
  </form>
  <form method="GET" action="/document/index">
    <input type="hidden" name="class_name" value="router">
    <input type="submit" value="router">
  </form>
  <form method="GET" action="/document/index">
    <input type="hidden" name="class_name" value="request">
    <input type="submit" value="request">
  </form>
  <form method="GET" action="/document/index">
    <input type="hidden" name="class_name" value="log">
    <input type="submit" value="log">
  </form>
  <form method="GET" action="/document/index">
    <input type="hidden" name="class_name" value="env">
    <input type="submit" value="env">
  </form>
  <form method="GET" action="/document/index">
    <input type="hidden" name="class_name" value="baseDbModel">
    <input type="submit" value="baseDbModel">
  </form>
  <form method="GET" action="/document/index">
    <input type="hidden" name="class_name" value="config">
    <input type="submit" value="config">
  </form>
  <form method="GET" action="/document/index">
    <input type="hidden" name="class_name" value="session">
    <input type="submit" value="session">
  </form>
  <form method="GET" action="/document/index">
    <input type="hidden" name="class_name" value="view">
    <input type="submit" value="view">
  </form>

</body>

</html>