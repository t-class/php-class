<!DOCTYPE html>
<html lang="ja">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Request FuncList</title>
</head>
<body>
  <h1>Request FuncList</h1>
  <ul>
    <li style="list-style: none;">
      <a href="/home/index">HOME</a>
      <a href="/document/index">DOC</a>
    </li>
  </ul>
  <?php
  echo "<hr>【get】getクエリ取得:";
    var_dump($this->_request->get('name'));
    echo "<hr>【post】postクエリ取得:";
    var_dump($this->_request->post('postName'));
    echo "<hr>【queryStr】クエリストリング取得:";
    var_dump($this->_request->queryStr());
    echo "<hr>【getKeyExistCheck】GETのキー名nameは存在するか:";
    var_dump($this->_request->getKeyExistCheck('name'));
    echo "<hr>【getKeyExistCheck】GETクエリストリングは存在するか:";
    var_dump($this->_request->getStrExistCheck());
    echo "<hr>【postKeyExistCheck】POSTのキー名nameは存在するか:";
    var_dump($this->_request->postKeyExistCheck('name'));
    echo "<hr>【postKeyExistCheck】POSTクエリストリングは存在するか:";
    var_dump($this->_request->postStrExistCheck());
    echo "<hr>【inSession】sessionに値保持:";
    var_dump($this->_request->inSession());
    echo "<hr>【resetSession】session['HTTP_METHOD_NAME']の値リセット:";
    var_dump($this->_request->resetSession());
    echo "<hr>【httpMethod】HTTPメソッド種別:";
    var_dump($this->_request->httpMethod());
    echo "<hr>【httpMethodCheck】HTTPメソッドチェック:";
    var_dump($this->_request->httpMethodCheck('POST'));
    echo "<hr>【userAgent】ユーザーエージェント:";
    var_dump($this->_request->userAgent());
    echo "<hr>【documentRoot】ドキュメントルート:";
    var_dump($this->_request->documentRoot());
    echo "<hr>";
    ?>
</body>
</html>