<?php

/**
 * $_funcListに機能名を書くことで、登録できる。
 */
class Dispatcher
{
  private static ?Dispatcher $_dispatcher = null;
  public static array $_instancefuncArray = [];
  private static array $_funcList = [
    "AutoLoad",
    "Router",
    // "Instance",
    // "View",
  ];

  private function __construct()
  {
    self::_onFunc();
  }
  public static function getInstance(): Dispatcher
  {
    if (is_null(self::$_dispatcher))
      self::$_dispatcher = new self();
    return self::$_dispatcher;
  }
  private static function _onFunc(): void
  {
    try {
      foreach (self::$_funcList as $func) {
        require_once sprintf("func/%s.php", $func);
        self::$_instancefuncArray[$func] = new $func();
      }
    } catch (Error $e) {
      Log::error($e);
      echo Config::err(0, 24) . "<hr>" . $func;
      exit;
    };
  }
  public final function __clone(): void
  {
    echo Config::err(0, 21);
    exit;
  }
}
