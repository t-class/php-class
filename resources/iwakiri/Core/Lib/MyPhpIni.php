<?php

class MyPhpIni
{
  private static ?MyPhpIni $_myPhpIni = null;
  public static function getInstance(): MyPhpIni
  {
    if (is_null(self::$_myPhpIni))
      self::$_myPhpIni = new self();
    return self::$_myPhpIni;
  }
  public static function set(string $directive, string|int $value): void
  {
    Log::info("PHPINI_SET_RESULT::" . strval(self::exe($directive, $value)));
  }
  private static function exe(string $directive, string|int $value): bool
  {
    return is_string(ini_set($directive, $value));
  }
  public final function __clone(): void
  {
    // echo Config::err(0, 21);
    exit;
  }
}
