<?php
class Log
{
  /**
   * Log::info('body');
   * [info,warning,error,unknown,trycatch]
   * 
   * @@class名取得できていない。
   */
  private static ?Log $_log = null;
  public static function getInstance(): Log
  {
    if (is_null(self::$_log))
      self::$_log = new self();
    return self::$_log;
  }

  private static function _outLog(string $log, string $logTag, string $fileName = "unknown", string $class = "unknown"): void
  {
    $logTag = Config::get('loglevel.' . $logTag);
    $logText = $logTag . '-' . Config::get('datetime') . ' [' . $class . '][' . $log . "] \n";
    $path = Config::get('path.log');

    if (!is_dir($path)) {
      if (mkdir($path, 0755, true)) {
        $logText .= "Log出力のフォルダが作成されました。PATH::" . $path . "\n";
      }
      echo Config::err(0, 10);
      exit;
    }

    $filePath = $path . Config::get('loglevel.' . $fileName) . Config::get('date') . ".log";

    if (error_log($logText, 3, $filePath)) {
      return;
    };
    echo Config::err(0, 28);
    exit;
  }
  public static function info(string $log, $logTag = __FUNCTION__): void
  {
    self::_outLog($log, $logTag, __FUNCTION__);
  }
  public static function warning(string $log, $logTag = __FUNCTION__): void
  {
    self::_outLog($log, $logTag, __FUNCTION__);
    self::info($log, $logTag);
  }
  public static function error(string $log, $logTag = __FUNCTION__): void
  {
    self::_outLog($log, $logTag, __FUNCTION__);
    self::warning($log, __FUNCTION__);
  }
  public static function unknown(string $log): void
  {
    self::_outLog($log, __FUNCTION__);
  }
  public static function trycatch(string $log): void
  {
    self::_outLog(self::_callFuncName($log), $log);
  }
  private static function _callFuncName($log): string
  {
    return strtolower(mb_strstr($log, ':', true));
  }

  public final function __clone(): void
  {
    echo Config::err(0, 21);
    exit;
  }
}
