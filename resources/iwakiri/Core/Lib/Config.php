<?php
class Config
{
  /**
   * // Config::err(7,01);
   * @@エラーより詳細にしたい。Config::err(0, 05)."パス"."ファイル名、キー名"
   * @@【未対応】Config::get('huga.piyo') のときに config/huga/piyo.php ファイルのデータを返す
   * @@【未対応】Config::get('foo.bar') のときに config/foo/bar.php と config/foo.php の bar キーの両方が存在するときはデフォルト値を返す
   * @param int $errCode
   * @param int $errNo
   * @return string
   */
  public static function err(int $errCode, int $errNo): string
  {
    return (require_once "../err/errList.php")[$errCode][$errNo];
  }
  public static function get(string $reqConfPath, string $default = null)
  {
    // @@キーかフォルダの判断ができていない
    // @@ファイル読み込みなどまとめる,メソッド化
    // @@ファイル見つけるメソッド、失敗したら後ろがキー名になる。
    // @@ファイル化Ｄか
    // @@エラー文出さないよう、()で閉じて
    try {
      // var_dump(self::isdir($reqConfPath));

      $confPathArray = explode(".", $reqConfPath);

      $confPath = array_shift($confPathArray);
      if (count($confPathArray) == 0) {
        $result = require(dirname(__DIR__, 2) . "/config/" . $confPath . ".php");
        if ($result) {
          return $result;
        }
      }
      if (count($confPathArray) > 0) {
        $confKey = array_pop($confPathArray);
        foreach ($confPathArray as $value) {
          $confPath .= "/" . $value;
        }
        $result = (require(dirname(__DIR__, 2) . "/config/" . $confPath . ".php"));
        if ($result && $result[$confKey]) {
          return $result[$confKey];
        }
      }
      return "unknown";
    } catch (Throwable $e) {
      Log::trycatch($e);
      echo self::err(0, 5);
      exit;
    }
  }
}
