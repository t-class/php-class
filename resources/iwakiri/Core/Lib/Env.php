<?php
class Env
{
  private static ?string $_reqKey = null;
  /**
   * Envクラスは、exampleからコピーして作成
   * Env::get("欲しい値の名前");で取得
   * getEnvFileでnginxの環境と同名のファイルのデータ取得
   * pointEnvで取得したデータからリクエストされた値を取得
   * getEnvValueで返す値を抽出
   *
   * @param string $reqKey
   * @return string
   */
  public static function get(string $reqKey): string
  {
    self::$_reqKey = trim($reqKey);
    return (self::getEnvValue(self::pointEnv(self::getEnvFile())));
  }
  /**
   * 環境を判断するため、サーバーの設定が必要
   *
   * @return array
   */
  public static function getEnvFile(): array
  {
    try {
      if (!isset($_SERVER["APP_ENV"])) {
        echo Config::err(0, 6);
        exit;
      }
      return file(Config::get("path.env") . $_SERVER["APP_ENV"]);
    } catch (Error) {
      echo Config::err(0, 1);
      exit;
    }
  }
  /**
   * 環境と同名のファイルのデータ取得
   *
   * @param array $envFileData
   * @param string $reqKey
   * @return string
   */
  public static function pointEnv(array $envFileData): string
  {
    return (implode(preg_grep('/^' . self::$_reqKey . '\=/', $envFileData)));
  }
  /**
   * 返す値を抽出
   *
   * @param string $pointEnv
   * @return string
   */
  public static function getEnvValue(string $pointEnv): string
  {
    if ($pointEnv == "") {
      echo Config::err(0, 8);
      exit;
    }
    preg_match('/(?<==).*/', $pointEnv, $value);
    return trim(implode($value));
  }
}
