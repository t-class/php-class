<?php
class MyPDO
{
  private static ?\PDO $_connection = null;
  private function __construct()
  {
    try {
      self::$_connection = new PDO(Config::get('mysql.content'), Config::get('mysql.user'), Config::get('mysql.password'));
      self::$_connection->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
    } catch (PDOException) {
      echo Config::err(7, 02);
      exit;
    }
  }
  /**
   * エラーコード出す=>問い合わせ=>サポートが調査
   * ER_702(hashimotosan)
   * 
   * .env使いたい
   * setAttributeの書き方工夫できる
   *
   * @return \PDO
   */
  public static function getInstance(): \PDO
  {
    if (is_null(self::$_connection))
      new self();
    return self::$_connection;
  }
  /**
   * 複製できないようにする
   *
   * @return void
   */
  public final function __clone()
  {
    echo Config::err(7, 05);
    exit;
  }
}
