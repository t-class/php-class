<?php
enum LogLevel: string
{
  case INFO = 'info';
  case WARNING = 'warning';
  case ERROR = 'error';
  case TEST = 'test';

  public function logLevel(LogLevel $v)
  {
    return match ($this) {
      self::INFO => '情報',
      self::WARNING => '警告',
      self::ERROR => 'エラー',
      self::TEST => 'test',
    };
  }
}
