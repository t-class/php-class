<?php

/**
 * Dispatcherのファイル自動読み込み機能
 * クラスが指定されてない場合、ファイル探して読み込む
 * 
 * 名前空間理解すると、splは引数にパスもらえる
 * @@名前空間を使用している場合、メソッド分けたい。
 */
class AutoLoad
{
  public function __construct()
  {
    spl_autoload_register([$this, '_myAutoLoad']);
  }

  private function _myAutoLoad(string $className): void
  {
    // $add = "/var/www/iwakiri/*";
    // $path = &$add;
    // do {
    //   $matchFilePath = implode(preg_grep('/\/' . $className . '.php$/', (glob(($path), GLOB_MARK))));

    //   if ($matchFilePath !== "") {
    //     require_once $matchFilePath;
    //     return;
    //   }

    //   $add .= "/*";
    //   echo $path . '<hr>';
    // } while (!$this->dirExistCheck($path));

    $path = "/var/www/iwakiri/*";
    while (!$this->dirExistCheck($path)) {
      $matchFilePath = implode(preg_grep('/\/' . $className . '.php$/', (glob(($path), GLOB_MARK))));
      if ($matchFilePath !== "") {
        require_once $matchFilePath;
        return;
      }
      $path .= "/*";
    }
  }

  private function dirExistCheck($path)
  {
    // emptyは空だけでなく、0とかも入るので、読み手が分かりずらい
    return (empty(preg_grep('//', glob($path, GLOB_ONLYDIR))));
  }
}
