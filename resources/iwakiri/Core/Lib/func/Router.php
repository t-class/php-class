<?php

/**
 * @@パラメータなどで、viewやapi等で処理分けをしたい
 * (new $controllerName(new View(new Request($controllerName, $method, $key))))->$method();
 */

class Router
{
  public function __construct()
  {
    $this->_route();
  }
  private function _route(): void
  {
    $controllerName = "home";
    $method = "index";
    $moveKey = "home";
    try {
      $splitUrl = preg_split("/\//", parse_url(rtrim($_SERVER['REQUEST_URI']), PHP_URL_PATH));
      if ($splitUrl !== false) {
        next($splitUrl) !== "" ? $controllerName = ucfirst(current($splitUrl)) . "Controller" : $controllerName = ucfirst($controllerName) . "Controller";
        if (current($splitUrl) !== "") $moveKey = current($splitUrl);
        next($splitUrl) ? $method = current($splitUrl) : $method;
      }

      (new $controllerName(new Request($controllerName, $method, $moveKey)))->$method();
      
    } catch (Exception $e) {
      header('Location: http://' . $_SERVER['HTTP_HOST'] . '/404.html');
      Log::info("index.php::called trycatch");
      throw new Exception();
    }
  }
}
