<?php
class View
{
  /**
   */
  private $_request = null;
  public $value = [];
  public function output(string $viewName): void
  {
    require_once Config::get("path.view") . $viewName . ".php";
  }
  public function setValue(mixed $name, mixed $value): void
  {
    $this->value[$name] = $value;
  }
  public function getValue(mixed $name): mixed
  {
    return $this->value[$name];
  }
}
