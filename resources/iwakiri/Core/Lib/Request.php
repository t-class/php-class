<?php

/**
 * 基本的にKey数字はすくない。
 * getの引数にintをいれても、エラーおこらない。
 * 
 * keyを判断するメソッドを作る。
 * 
 * apiの時、GETやPOSTリクエストで、string以外の値が来る場合もある
 * json受け取りたい。
 */
class Request
{
  public string $http_method = "";
  public string $controllerName = "";
  public string $method = "";
  public string $moveKey = "";

  public function __construct($controllerName = "", $method = "", $moveKey = "")
  {
    $this->controllerName = $controllerName;
    $this->method = $method;
    $this->moveKey = $moveKey;
    $this->http_method = $_SERVER['REQUEST_METHOD'];
  }
  public function get(?string $key = null): string|array
  {
    if (($_GET !== [] || !isset($_GET[$key]))) {
      if (is_null($key)) return $_GET;
      return $_GET[$key];
    }
    return "";
  }
  public function post(?string $key = null): string|array
  {
    if (($_POST !== [] || !isset($_POST[$key]))) {
      if (is_null($key)) return $_POST;
      return $_POST[$key];
    }
    return "";
  }
  public function queryStr(?string $key = null): string|array
  {
    $httpMethodName = mb_strtolower($this->http_method);
    return $this->$httpMethodName($key);
  }
  public function getStrExistCheck(): bool
  {
    return $_GET !== [];
  }
  public function getKeyExistCheck(string $key): bool
  {
    return $_GET !== [] && isset($_GET[$key]);
  }
  public function postStrExistCheck(): bool
  {
    return $_POST !== [];
  }
  public function postKeyExistCheck(string $key): bool
  {
    return $_POST !== [] && isset($_POST[$key]);
  }
  // public function jsonGet(): array
  // {
  //   return [];
  // }
  public function inSession(): void
  {
    $_SESSION[$this->http_method] = $this->queryStr();
  }
  public function resetSession(): void
  {
    $_SESSION[$this->http_method] = [];
  }
  public function httpMethod(): string
  {
    return $_SERVER['REQUEST_METHOD'];
  }
  public function httpMethodCheck($http_method): bool
  {
    return $_SERVER['REQUEST_METHOD'] === $http_method;
  }
  public function userAgent(): string
  {
    return $_SERVER['HTTP_USER_AGENT'];
  }
  public function documentRoot(): string
  {
    return $_SERVER['DOCUMENT_ROOT'];
  }
}
