<?php

/**
 * 乗っ取られ対策できていない
 * 細かく、id書き換え
 * session_regenerate_id
 * 
 * 
 * password_hash()等を使い、重要な情報は暗号化する。
 * ファイル自体をハッシュする。
 * id知られた場合、特定される。
 * 
 * ローカルストレージ、クッキーは別で作ればいい
 * 
 * session_name()で、クッキーのセッション名をランダムに変えるといい
 * 
 * session_commit
 * 
 */
class Session implements SessionHandlerInterface
{
  private $savePath;
  public function __construct()
  {
    Log::info("session::new");
    $this->savePath = Config::get('path.session');
  }

  public function open($savePath, $sessionName): bool
  {
    $_SESSION['HTTP_REQ_HEADERS'] = getallheaders();
    Log::info("session::open::" . $savePath . $sessionName);
    if (!empty($savePath)) {
      $this->savePath = $savePath;
    }
    return true;
  }

  public function close(): bool
  {
    Log::info("session::close");
    return true;
  }
  #[\ReturnTypeWillChange]

  public function read($id): bool|string
  {
    Log::info("session::read::" . $id);
    return (string)@file_get_contents("$this->savePath/sess_$id");
  }

  public function write($id, $data): bool
  {
    Log::info("session::write");
    return file_put_contents("$this->savePath/sess_$id", $data) === false ? false : true;
  }

  public function destroy($id): bool
  {
    $file = "$this->savePath/sess_$id";
    if (file_exists($file)) {
      unlink($file);
    }
    $_SESSION = [];
    return true;
  }

  #[\ReturnTypeWillChange]
  public function gc($maxlifetime): bool|int
  {
    Log::info("session期限" . $maxlifetime);
    foreach (glob("$this->savePath/sess_*") as $file) {
      if (filemtime($file) + $maxlifetime < time() && file_exists($file)) {
        unlink($file);
      }
    }
    return true;
  }
}

$handler = new Session();
session_set_save_handler($handler, true);

// callable: open(string $savePath, string $sessionName): bool open 
// コールバックはクラスのコンストラクタのようなもので、セッションを開くときに実行されます。 
// セッションが自動で開始したり、あるいは手動で session_start() で開始させたりするときに、最初に実行されるコールバック関数がこれです。
