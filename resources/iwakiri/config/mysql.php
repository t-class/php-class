<?php
return [
  "port" => Env::get("MYSQL_PORT"),
  "database" => Env::get("MYSQL_DATABASE"),
  "user" => Env::get("MYSQL_USER"),
  "password" => Env::get("MYSQL_PASSWORD"),
  "root_password" => Env::get("MYSQL_ROOT_PASSWORD"),
  "content" => Env::get("MYSQL_CONTENT"),
];
