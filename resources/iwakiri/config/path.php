<?php
$s = DIRECTORY_SEPARATOR;
return [
  // default
  // "root" => "/var/www/",
  // "log"=> "/var/www/tmp/log/",
  // "env"=>"/var/www/.env/",
  // "log" => "{$s}var{$s}www{$s}iwakiri{$s}tmp{$s}log{$s}",
  "root" => "{$s}var{$s}www{$s}iwakiri{$s}",
  "env" => "{$s}var{$s}www{$s}iwakiri{$s}.env{$s}",
  "log" => "{$s}var{$s}www{$s}iwakiri{$s}tmp{$s}log{$s}" . date("y") . $s . date("m") . $s,
  "session" => "{$s}var{$s}www{$s}iwakiri{$s}tmp{$s}session",
  "controller" => "{$s}var{$s}www{$s}iwakiri{$s}Core{$s}Controllers{$s}",
  "view" => "{$s}var{$s}www{$s}iwakiri{$s}Core{$s}Views{$s}",
  "css" => "{$s}var{$s}www{$s}iwakiri{$s}Core{$s}Views{$s}css{$s}",
];
