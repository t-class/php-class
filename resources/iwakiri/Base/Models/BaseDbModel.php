<?php

class BaseDbModel
{
  private ?\PDO $_connection = null;
  public function __construct()
  {
    $this->_connection = MyPDO::getInstance();
  }
  /**
   * SELECT文
   * エラーハンドリング。
   * 
   *
   * @param string $sql
   * @return array|string
   */
  public function select(string $sql): array|string
  {
    $result = $this->_connection->query($sql);
    if ($result) {
      return $result->fetchAll(PDO::FETCH_ASSOC);
    } else {
      echo Config::err(7, 77);
      exit;
    }
  }
}
