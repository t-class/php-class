<?php
// エラーハンドラ関数
// https://www.php.net/manual/ja/function.set-error-handler.php

set_error_handler('my_error_handler');

function my_error_handler($errno, $errstr, $errfile, $errline)
{
  if (!(error_reporting() & $errno)) {
    // error_reporting 設定に含まれていないエラーコードのため、
    // 標準の PHP エラーハンドラに渡されます。
    return;
  }

  // Fatal Errorを捕捉する方法
  // https://gist.github.com/koshiaaaaan/903173


  $errstr = htmlspecialchars($errstr);

  switch ($errno) {
    case E_ERROR:
      Log::error($errstr);
      var_dump($errstr);
      exit;
    case E_WARNING:
      Log::warning($errstr);
      var_dump($errstr);
      // exit;
  }

  /* PHP の内部エラーハンドラを実行しません */
  return true;
};

register_shutdown_function(function () {
  global  $errors;

  // バッファの取得
  $ob = ob_get_clean();
  // シャットダウン前に起こったエラーを取得
  $error = error_get_last();

  // エラーがあれば自作のエラーハンドラを呼び出す
  if (!empty($error)) {
    if (my_error_handler(
      $error['type'],
      $error['message'],
      $error['file'],
      $error['line'],
    ));
  }
});
