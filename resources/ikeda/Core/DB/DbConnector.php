<?php
//require_once(dirname(__DIR__) . '/../Libs/Env.php');
class DbConnector {
    private static array $_dsn;
    private static ?\PDO $_connection = null;

    private function __construct(){
        #DSN取得
        self::$_dsn = [
            'user'     => Env::get('MYSQL_USER'),
            'password' => Env::get('MYSQL_PASSWORD'),
            'content'  => sprintf("%s:dbname=%s;host=%s;charset=%s", Env::get('DB_SERVER'), 
                                  Env::get('MYSQL_DATABASE'), Env::get('MYSQL_HOST'), Env::get('MYSQL_CHARSET'))
        ];

        #PDOクラスのインスタンス生成
        #setAttributeはtry, catchする時に毎回書く必要がある
        try{
            self::$_connection = new \PDO(self::$_dsn['content'], self::$_dsn['user'], self::$_dsn['password']);
            self::$_connection->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
        }
        catch(\PDOException $e){
            #エラーメッセージ検討の必要性
            echo $e->getMessage();
        }
    }

    #PDOクラスのインスタンス取得
    public static function getInstance(): \PDO{
        if(is_null(self::$_connection)){
            new self();
        }
        return self::$_connection;
    }

    #クローンの制御
    #final publicでも動きはする
    public final function __clone(): void{
        throw new \RuntimeException(get_class($this).'クラスではインスタンスの複製は許可されていません。');
    }

    #unserializeの制御
    public final function __wakeup(){
        throw \Exception(get_class($this).'クラスではアンシリアライズは許可されていません。');
    }
}