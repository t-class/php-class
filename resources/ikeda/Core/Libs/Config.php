<?php

class Config {

    private static int $_arrPt;
    private static array $_eachParam;

    public static function get(string $recvParam, ?string $default = null): mixed{

        $confDirPath = self::getConfDirPath($recvParam);
        $tmpFilePath = $confDirPath . sprintf('/%s.php', self::$_eachParam[self::$_arrPt]);

        if(file_exists($tmpFilePath)){
            $confData = include($tmpFilePath);
            #キーの指定が終わるまで繰り返し
            for($i = self::$_arrPt+1; $i < count(self::$_eachParam); $i++){
                #一時的に保存
                $tmpData = $confData;
                #キーが存在したら、そのキーの値（配列）を取得
                if(array_key_exists(self::$_eachParam[$i], $tmpData)){
                    $key = self::$_eachParam[$i];
                    $confData = [];
                    $confData = $tmpData[$key];
                    continue;
                }
                return self::getDefault($default);
            }
            #/config/ディレクトリ/phpファイルが存在する時、/config/phpファイル/配列のキーも存在するかチェック
            if(self::existFileAndKey($confDirPath)){
                return self::getDefault($default);
            }
            return $confData;
        }
        else{
            return self::getDefault($default);
        }
    }

    private static function getConfDirPath(string $recvParam): string{

        $confDirPath = realpath(dirname(__DIR__) . '/config');
        #configディレクトリの存在確認
        if(!is_dir($confDirPath)){
            echo "configディレクトリが存在しません。";
            exit;
        }
        #パラメータを分解
        $eachParam = explode(".", $recvParam);
        self::$_eachParam = $eachParam;

        #ディレクトリが存在しなくなるま判定繰り返し
        for($ep = 0; $ep < count($eachParam); $ep++){
            #dファイルパスを一時的に保持
            $tmpPath = $confDirPath . sprintf('/%s', $eachParam[$ep]);
            #ディレクトリの場合処理を継続
            if(is_dir($tmpPath)){
                if(array_key_exists($ep+1, $eachParam)){
                    $confDirPath = $tmpPath;
                    continue;
                }
            }
            break;
        }
        self::$_arrPt = $ep;
        return $confDirPath;
    }

    private static function existFileAndKey($confDirPath): bool{
        $filePath = $confDirPath . ".php";
        if(file_exists($filePath)){
            $data = include($filePath);
            if(array_key_exists(self::$_eachParam[self::$_arrPt], $data)){
                return true;
            }
        }
        return false;
    }

    private static function getDefault(?string $default): mixed{
        if(is_null($default)){
            return "NO DATA FOUND";
        }
        return $default;
    }
}
