<?php

namespace Core\Libs;

class Env {
    
    public static array $envData = [];
    public static function get(string $key) : mixed {
        if(empty(self::$envData)){
            $filePath = self::getFilePath();
            self::fetchData($filePath);
        }
        return self::$envData[$key];
    }

    private static function getFilePath() : string {
        if(!array_key_exists("APP_ENV", $_SERVER)){
            echo "Check out 'defalt.conf', Key : 'APP_ENV'";
            exit;
        }
        $filePath = realpath(sprintf("%s/.env/%s", dirname(__DIR__, 2), $_SERVER['APP_ENV']));
        #ファイルが存在しない時はexit
        if(is_dir($filePath) || !file_exists($filePath)){
            echo "No Such File";
            exit;
        }
        return $filePath;
    }

    private static function fetchData(string $filePath) :void{
        $dataArr = file($filePath, FILE_SKIP_EMPTY_LINES);
        foreach($dataArr as $line){
            $newLine = preg_replace("/(\s)/", "", $line);
            if(str_starts_with($newLine, "#")) continue;
            $data = explode("=", $newLine, 2);
            self::$envData[current($data)] = next($data);
        }
    }
}