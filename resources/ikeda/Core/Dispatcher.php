<?php
namespace Core;

class Dispatcher {

    private static ?Dispatcher $_disp = null;
    public string $projectDir = ''; 

    private function __construct() {
        $this->projectDir = dirname(__DIR__);
        $this->registMethod();
    }

    private function registMethod() : void {
        spl_autoload_register([$this, "loadClass"]);
    }

    private function loadClass(string $className) : void {
        $filePath = $this->toReadablePath($className);

        if(!is_file($filePath)){
            sprintf("ファイルが存在しません : %s", $filePath);
            exit;
        }
        require $filePath;
    }

    protected function toReadablePath(string $class) : string {
        $filePath = sprintf('%s/%s.php', $this->projectDir, $class);
        return str_replace('\\', DIRECTORY_SEPARATOR, $filePath);
    }

    #自クラスのインスタンス生成
    public static function getInstance(): Dispatcher{
        if(is_null(self::$_disp)){
            self::$_disp = new self();
        }
        return self::$_disp;
    }

    #クローンの制御
    public final function __clone(): void{
        throw new \RuntimeException(get_class($this).'クラスではインスタンスの複製は許可されていません。');
    }

    #unserializeの制御
    public final function __wakeup(){
        throw \Exception(get_class($this).'クラスではアンシリアライズは許可されていません。');
    }
}